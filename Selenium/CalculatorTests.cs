using FluentAssertions;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;

namespace Selenium
{
    public class CalculatorTests
    {
        private ChromeDriver _driver;

        [SetUp]
        public void SetUp()
        {
            _driver = new ChromeDriver("�cie�ka do drivera");
        }

        [TearDown]
        public void TearDown()
        {
            _driver.Quit();
        }

        [Test]
        public void ResultOf_MultiplyingTwoTimesTwo_Should_BeFour()
        {
            //arrange
            _driver.Url = "http://www.anaesthetist.com/mnm/javascript/calc.htm";
            var twoButton = _driver.FindElementByName("two");
            var mulButton = _driver.FindElementByName("mul");
            var resultButton = _driver.FindElementByName("result");
            var displayInput = _driver.FindElementByName("Display");

            // act
            twoButton.Click();
            mulButton.Click();
            twoButton.Click();
            resultButton.Click();

            var screenshot = _driver.GetScreenshot();

            //assert
            var displayedValue = displayInput.GetAttribute("value");
            var calculationResult = int.Parse(displayedValue);
            calculationResult.Should().Be(4);
        }
    }
}